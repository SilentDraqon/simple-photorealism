################################################################################
#   Any Node Exists
################################################################################

def anyNodeExists(context, type):
    if(context.object.active_material.node_tree.nodes.get("ImageBaseColor", None) is None 
        and context.object.active_material.node_tree.nodes.get("ImageAmbientOcclusion", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageOpacity", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageMetallic", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageNormal", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageRoughness", None) is None):
            return False
    elif((type == "node" 
        and context.object.active_material.node_tree.nodes.get("ImageBaseColor", None) is not None
        and context.object.active_material.node_tree.nodes.get("ImageAmbientOcclusion", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageOpacity", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageMetallic", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageNormal", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageRoughness", None) is None)
        or (context.object.active_material.node_tree.nodes.get("ImageBaseColor", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageAmbientOcclusion", None) is not None
        and context.object.active_material.node_tree.nodes.get("ImageOpacity", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageMetallic", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageNormal", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageRoughness", None) is None)):
            return False
    else:    
        return True