################################################################################
#   Update Opacity Gamma
################################################################################

def updateOpacityGamma(self, context):
    gammanode = context.object.active_material.node_tree.nodes.get("GammaOpacity")
    if gammanode is not None:
        gammanode.inputs[1].default_value = context.scene.prop_OpacityGamma