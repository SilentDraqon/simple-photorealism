################################################################################
#   Update Displace Strength
################################################################################

def updateDisplaceStrength(self, context):
    Modifier = context.object.modifiers.get("Displace", None)
    if Modifier is not None:
        Modifier.strength = context.scene.prop_DisplaceStrength