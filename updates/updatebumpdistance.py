################################################################################
#   Update Bump Distance
################################################################################

def updateBumpDistance(self, context):
    bumpnode = context.object.active_material.node_tree.nodes.get("BumpNormal")
    if bumpnode is not None:
        bumpnode.inputs[1].default_value = context.scene.prop_BumpDistance