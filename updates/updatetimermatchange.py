################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy

# Addon Operators
from ..variables.timermatchange import TimerMatChange


################################################################################
#   Update TimerMatChange
################################################################################

timer = TimerMatChange()
def updateTimerMatChange():
    global timer
    if(bpy.context.object is not None):
        if(timer.x is not bpy.context.object):
            bpy.ops.object.getmaterial_operator()
            if(bpy.context.object.active_material is not None):
                bpy.context.scene.prop_MatName = bpy.context.object.active_material.name
            timer.x = bpy.context.object
    return 3.0