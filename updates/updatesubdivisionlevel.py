################################################################################
#   Update Subdivision Level
################################################################################

def updateSubdivisionLevel(self, context):
    Object = context.object
    Modifiers = Object.modifiers
    SubdivisionLevel = context.scene.prop_SubdivisionLevel
    SubdivisionModifier = Modifiers.get("Subdivide", None)
    if(SubdivisionLevel < 1):
        if SubdivisionModifier is not None:
            Modifiers.remove(SubdivisionModifier)
    else:
        if SubdivisionModifier is None:
            SubdivisionModifier = Modifiers.new("Subdivide", "SUBSURF")
        SubdivisionModifier.levels = SubdivisionLevel