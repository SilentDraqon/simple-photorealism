################################################################################
#   Update Displace Midlevel
################################################################################

def updateDisplaceMidlevel(self, context):
    Modifier = context.object.modifiers.get("Displace", None)
    if Modifier is not None:
        Modifier.mid_level = context.scene.prop_DisplaceMidlevel