################################################################################
#   Update Texture Scale X
################################################################################

def updateTextScaleX(self, context):
    Modifier = context.object.modifiers.get("Displace", None)
    if Modifier is not None:
        Modifier.texture.repeat_x = context.scene.prop_TexScaleX
    mappingnode = context.object.active_material.node_tree.nodes.get("Mapping")
    if mappingnode is not None:
        mappingnode.inputs[3].default_value = (context.scene.prop_TexScaleX, mappingnode.inputs[3].default_value.y, mappingnode.inputs[3].default_value.z)