################################################################################
#   Update Texture Scale Y
################################################################################

def updateTextScaleY(self, context):
    Modifier = context.object.modifiers.get("Displace", None)
    if Modifier is not None:
        Modifier.texture.repeat_y = context.scene.prop_TexScaleY
    mappingnode = context.object.active_material.node_tree.nodes.get("Mapping")
    if mappingnode is not None:
        mappingnode.inputs[3].default_value = (mappingnode.inputs[3].default_value.x, context.scene.prop_TexScaleY, mappingnode.inputs[3].default_value.z)