################################################################################
#   Update Texture Offset Y
################################################################################

def updateTextOffsetY(self, context):
    mappingnode = context.object.active_material.node_tree.nodes.get("Mapping")
    if mappingnode is not None:
        mappingnode.inputs[1].default_value = (mappingnode.inputs[1].default_value.x, context.scene.prop_TexOffsetY, mappingnode.inputs[1].default_value.z)