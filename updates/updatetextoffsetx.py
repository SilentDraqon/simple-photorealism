################################################################################
#   Update Texture Offset X
################################################################################

def updateTextOffsetX(self, context):
    mappingnode = context.object.active_material.node_tree.nodes.get("Mapping")
    if mappingnode is not None:
        mappingnode.inputs[1].default_value = (context.scene.prop_TexOffsetX, mappingnode.inputs[1].default_value.y, mappingnode.inputs[1].default_value.z)