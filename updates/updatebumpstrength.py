################################################################################
#   Update Bump
################################################################################

def updateBumpStrength(self, context):
    bumpnode = context.object.active_material.node_tree.nodes.get("BumpNormal")
    if bumpnode is not None:
        bumpnode.inputs[0].default_value = context.scene.prop_BumpStrength