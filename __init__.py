################################################################################
#   Package Information
################################################################################

bl_info = {
    "name": "simple-pbr",
    "blender": (2, 92, 0),
    "category": "User Interface",
    "version": (0, 1, 0, 0),
    "location": "VIEW_3D",
}


################################################################################
#   Imported Libraries
################################################################################

# Reload Addon Operators
if "bpy" in locals():
    import importlib
    importlib.reload(OperatorFileBrowser)
    importlib.reload(OperatorFileRemove)
    importlib.reload(OperatorMaterialGet)
    importlib.reload(OperatorMaterialRename)
    importlib.reload(OperatorMaterialCreate)
    importlib.reload(OperatorMaterialRemove)
    importlib.reload(TimerMatChange)
    importlib.reload(MainPanel)
    importlib.reload(SubPanelTextureSettings)
    importlib.reload(SubPanelMaterialSettings)
    importlib.reload(SubPanelMaterialSettings_Modifiers)
    importlib.reload(SubPanelMaterialSettings_Nodes)
    importlib.reload(SubPanelMaterialSettings_Textures)

# Import Addon Operators
else:
    from .panels.mainpanel import MainPanel
    from .panels.subpanelmaterialsettings import SubPanelMaterialSettings
    from .panels.subpanelmaterialsettings_modifiers import SubPanelMaterialSettings_Modifiers
    from .panels.subpanelmaterialsettings_nodes import SubPanelMaterialSettings_Nodes
    from .panels.subpanelmaterialsettings_textures import SubPanelMaterialSettings_Textures
    from .panels.subpaneltexturesettings import SubPanelTextureSettings
    from .operators.filebrowser import OperatorFileBrowser
    from .operators.fileremove import OperatorFileRemove
    from .operators.materialrename import OperatorMaterialRename
    from .operators.materialremove import OperatorMaterialRemove
    from .operators.materialcreate import OperatorMaterialCreate
    from .operators.materialget import OperatorMaterialGet
    from .variables.timermatchange import TimerMatChange
    from .updates.updateambientocclusion import updateAmbientOcclusion
    from .updates.updatebumpstrength import updateBumpStrength
    from .updates.updatetimermatchange import updateTimerMatChange
    from .updates.updatebumpdistance import updateBumpDistance
    from .updates.updatebumpinvert import updateBumpInvert
    from .updates.updatedisplacemidlevel import updateDisplaceMidlevel
    from .updates.updatedisplacestrength import updateDisplaceStrength
    from .updates.updatemetallicgamma import updateMetallicGamma
    from .updates.updateopacitygamma import updateOpacityGamma
    from .updates.updateroughnessgamma import updateRoughnessGamma
    from .updates.updatesubdivisionlevel import updateSubdivisionLevel
    from .updates.updatetextscalex import updateTextScaleX
    from .updates.updatetextscaley import updateTextScaleY
    from .updates.updatetextoffsetx import updateTextOffsetX
    from .updates.updatetextoffsety import updateTextOffsetY

# Internal Libraries
import bpy
from bpy.props import EnumProperty, StringProperty, BoolProperty, IntProperty, FloatProperty
from bpy.types import Scene
from bpy.utils import register_class, unregister_class


################################################################################
#   Registration
################################################################################

classes = [
    OperatorFileBrowser,
    OperatorFileRemove,
    OperatorMaterialGet,
    OperatorMaterialRename,
    OperatorMaterialCreate,
    OperatorMaterialRemove,
    MainPanel,
    SubPanelTextureSettings,
    SubPanelMaterialSettings,
    SubPanelMaterialSettings_Modifiers,
    SubPanelMaterialSettings_Textures,
    SubPanelMaterialSettings_Nodes
]

def register():

    # Register Classes
    for operator in classes:
        register_class(operator)

    # Register Timers
    bpy.app.timers.register(updateTimerMatChange)

    # Register Properties
    Scene.prop_MapDropDown = EnumProperty(
        name="Select Map",
        items=[
        ("BaseColor", "Base Color", '', 'IMAGE', 0),
        ("AmbientOcclusion", "Ambient Occlusion", '', 'IMAGE', 1),
        ("Roughness", "Roughness", '', 'IMAGE', 2),
        ("Normal", "Normal", '', 'IMAGE', 3),
        ("Height", "Height", '', 'IMAGE', 4),
        ("Metallic", "Metallic", '', 'IMAGE', 5),
        ("Opacity", "Opacity", '', 'IMAGE', 6),
    ],
        description="...",
        default="BaseColor"
    )
    Scene.prop_SubdivisionLevel = IntProperty(
        name="Subdivision Level",
        description="...",
        default=0,
        min=0,
        max=12,
        update=updateSubdivisionLevel
    )
    Scene.prop_AOFac = FloatProperty(
        name="Ambient Occlusion Factor",
        description="...",
        default=0.500,
        min=0.000,
        max=1.000,
        update=updateAmbientOcclusion
    )
    Scene.prop_RoughnessGamma = FloatProperty(
        name="Roughness Gamma",
        description="...",
        default=1.000,
        min=0.000,
        max=10.000,
        update=updateRoughnessGamma
    )
    Scene.prop_MetallicGamma = FloatProperty(
        name="Metallic Gamma",
        description="...",
        default=1.000,
        min=0.000,
        max=10.000,
        update=updateMetallicGamma
    )
    Scene.prop_OpacityGamma = FloatProperty(
        name="Opacity Gamma",
        description="...",
        default=1.000,
        min=0.000,
        max=10.000,
        update=updateOpacityGamma
    )
    Scene.prop_BumpStrength = FloatProperty(
        name="Bump Strength",
        description="...",
        default=0.500,
        min=0.000,
        max=1.000,
        update=updateBumpStrength
    )
    Scene.prop_DisplaceStrength = FloatProperty(
        name="Displacement Strength",
        description="...",
        default=0.500,
        min=-2.000,
        max=2.000,
        update=updateDisplaceStrength
    )
    Scene.prop_DisplaceMidlevel = FloatProperty(
        name="Displacement Midlevel",
        description="...",
        default=0.500,
        min=0.000,
        max=1.000,
        update=updateDisplaceMidlevel
    )
    Scene.prop_BumpInvert = BoolProperty(
        name="Bump Invert",
        description="...",
        default=False,
        update=updateBumpInvert
    )
    Scene.prop_BumpDistance = FloatProperty(
        name="Bump Distance",
        description="...",
        default=0.500,
        min=0.000,
        max=10.000,
        update=updateBumpDistance
    )
    Scene.prop_TexScaleX = IntProperty(
        name="Texture Scale X",
        description="...",
        default=1,
        min=0,
        max=10,
        update=updateTextScaleX
    )
    Scene.prop_TexScaleY = IntProperty(
        name="Texture Scale Y",
        description="...",
        default=1,
        min=0,
        max=10,
        update=updateTextScaleY
    )
    Scene.prop_TexOffsetX = FloatProperty(
        name="Texture Offset X",
        description="...",
        default=0.000,
        min=-10.000,
        max=10.000,
        update=updateTextOffsetX
    )
    Scene.prop_TexOffsetY = FloatProperty(
        name="Texture Offset Y",
        description="...",
        default=0.000,
        min=-10.000,
        max=10.000,
        update=updateTextOffsetY
    )
    Scene.prop_MatName = StringProperty(
        name="",
        description="...",
        default="Create New Material:"
    )

def unregister():
    
    # Unregister Classes
    for operator in classes:
        unregister_class(operator)
    
    # Unregister Timers
    bpy.app.timers.unregister(updateTimerMatChange)

    # Unregister Properties
    del Scene.prop_MapDropDown
    del Scene.prop_MatName
    del Scene.prop_SubdivisionLevel
    del Scene.prop_AOFac
    del Scene.prop_TexScaleX
    del Scene.prop_TexScaleY
    del Scene.prop_TexOffsetX
    del Scene.prop_TexOffsetY
    del Scene.prop_BumpDistance
    del Scene.prop_BumpInvert
    del Scene.prop_BumpStrength
    del Scene.prop_DisplaceStrength
    del Scene.prop_DisplaceMidlevel
    del Scene.prop_RoughnessGamma
    del Scene.prop_MetallicGamma
    del Scene.prop_OpacityGamma

if __name__ == "__main__":
    register()
