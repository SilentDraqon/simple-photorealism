################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy
from bpy.props import StringProperty
from bpy.types import Operator
from ..getter.anyNodeExists import anyNodeExists


################################################################################
#   File Remove Operator
################################################################################

class OperatorFileRemove(Operator):
    bl_idname = "object.reset_operator"
    bl_label = "ResetOperator"
    MapName: StringProperty()

    @classmethod
    def poll(self, context):
        return context.object is not None

    def execute(self, context):
        FullMaterialName = context.object.active_material.name +"_"+ self.MapName
        nodes = context.object.active_material.node_tree.nodes
        tree = context.object.active_material.node_tree
        outputnode = nodes.get("Principled BSDF")

        node = nodes.get("Image"+self.MapName, None)
        if node is not None:
            nodes.remove(node)
        
        Image = bpy.data.images.get(FullMaterialName, None)
        if Image is not None:
            bpy.data.images.remove(Image)

        if str(self.MapName) == "BaseColor":
            print("handle node")
            aonode = nodes.get("ImageAmbientOcclusion")
            mixrgbnode = nodes.get("MixRGBAO", None)
            if mixrgbnode is not None and aonode is None:
                nodes.remove(mixrgbnode)
            if nodes.get("MixRGBAO", None) is None and aonode is not None:
                tree.links.new(aonode.outputs[0], outputnode.inputs[0])

        if str(self.MapName) == "AmbientOcclusion":
            print("handle node")
            basenode = nodes.get("ImageBaseColor")
            mixrgbnode = nodes.get("MixRGBAO", None)
            if mixrgbnode is not None and basenode is None:
                nodes.remove(mixrgbnode)
            if nodes.get("MixRGBAO", None) is None and basenode is not None:
                tree.links.new(basenode.outputs[0], outputnode.inputs[0])

        if str(self.MapName) == "Roughness":
            print("handle node")
            gammanode = nodes.get("Gamma"+self.MapName, None)
            if gammanode is not None:
                nodes.remove(gammanode)

        if str(self.MapName) == "Normal":
            print("handle node")
            bumpnode = nodes.get("Bump"+self.MapName, None)
            if bumpnode is not None:
                nodes.remove(bumpnode)

        if str(self.MapName) == "Metallic":
            print("handle node")
            gammanode = nodes.get("Gamma"+self.MapName, None)
            if gammanode is not None:
                nodes.remove(gammanode)

        if str(self.MapName) == "Opacity":
            print("handle node")
            gammanode = nodes.get("Gamma"+self.MapName, None)
            if gammanode is not None:
                nodes.remove(gammanode)

        if(context.object.active_material.node_tree.nodes.get("ImageBaseColor", None) is None 
        and context.object.active_material.node_tree.nodes.get("ImageAmbientOcclusion", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageOpacity", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageMetallic", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageNormal", None) is None
        and context.object.active_material.node_tree.nodes.get("ImageRoughness", None) is None):
            coordsnode = nodes.get("Coords", None)
            mappingnode = nodes.get("Mapping", None)
            if coordsnode is not None:
                nodes.remove(coordsnode)
            if mappingnode is not None:
                nodes.remove(mappingnode)

        # Height Map !
        if str(self.MapName) == "Height":
            Texture = bpy.data.textures.get(FullMaterialName, None)
            if Texture is not None:
                bpy.data.textures.remove(Texture)
            Modifier = context.object.modifiers.get("Displace", None)
            if Modifier is not None:
                context.object.modifiers.remove(Modifier)


        return {'FINISHED'}