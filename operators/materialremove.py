################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy
from bpy.types import Operator


################################################################################
#   Material Remove Operator
################################################################################

class OperatorMaterialRemove(Operator):
    bl_idname = "object.matremove_operator"
    bl_label = "MatName Operator"
    unlinkOnly: bpy.props.BoolProperty()

    @classmethod
    def poll(self, context):
        return context.object is not None

    def execute(self, context):
        if self.unlinkOnly is False:
            bpy.data.materials.remove(context.object.active_material)
        else:
            mat_dict = {mat.name: i for i, mat in enumerate(context.object.data.materials)}
            bpy.context.object.data.materials.pop(index=mat_dict[context.object.active_material.name])

        return {'FINISHED'}