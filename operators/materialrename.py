################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy
from bpy.types import Operator


################################################################################
#   Material Rename Operator
################################################################################

class OperatorMaterialRename(Operator):
    bl_idname = "object.matname_operator"
    bl_label = "MatName Operator"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def execute(self, context):
        Texture = bpy.data.textures.get(context.object.active_material.name + "_Height", None)

        if(context.object.active_material is not None):
            mapnames  = ["BaseColor", "AmbientOcclusion", "Roughness", "Metallic", "Opacity", "Normal", "Height"]
            for mapname in mapnames:
                oldname = context.object.active_material.name + "_" + mapname
                if(context.object.active_material.node_tree.nodes.get("Image"+mapname) is not None):
                    newname = context.scene.prop_MatName + "_" + mapname
                    mapnode = context.object.active_material.node_tree.nodes.get("Image"+mapname)
                    mapnode.image.name = newname

            context.object.active_material.name = context.scene.prop_MatName

        Image = bpy.data.images.get(context.object.active_material.name + "_Height", None)
        if Image is not None:
            Image.name = context.scene.prop_MatName + "_Height"

        if Texture is not None:
            Texture.image = Image
            Texture.name = context.scene.prop_MatName + "_Height"
            if context.object.modifiers.get("Displace", None) is None:
                context.object.modifiers.new("Displace", "DISPLACE")
                context.object.modifiers["Displace"].texture = Texture

        return {'FINISHED'}