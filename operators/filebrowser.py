################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy
from bpy.props import StringProperty
from bpy.types import Operator
from bpy_extras.io_utils import ImportHelper


################################################################################
#   File Browser Operator
################################################################################

class OperatorFileBrowser(Operator, ImportHelper):
    bl_idname = "test.open_filebrowser"
    bl_label = "Open the file browser"
    MapName: StringProperty()

    @classmethod
    def poll(self, context):
        return context.object is not None

    filter_glob: StringProperty(
        default='*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.bmp', options={'HIDDEN'}
    )

    def createOrGetNode(self, context, nodetype, nodename):
        nodes = context.object.active_material.node_tree.nodes
        node = nodes.get(nodename, None)
        if(node is None):
            node = nodes.new(nodetype)
        node.label = nodename
        node.name = nodename
        return node

    def execute(self, context):
        FullMaterialName = context.object.active_material.name +"_"+ self.MapName

        Image = bpy.data.images.get(FullMaterialName, None)
        if Image is None:
            Image = bpy.data.images.load(self.filepath, check_existing=False)
        Image.name = FullMaterialName
        Image.filepath = self.filepath

        matname = context.object.active_material.name
        mat = bpy.data.materials.get(matname)
        if mat is None:
            mat = bpy.data.materials.new(name=matname)
        if context.object.data.materials:
            if(context.object.data.materials.get(matname, None) is None):
                context.object.active_material = mat
            else: 
                context.object.material_slots[matname].material = mat
        else:
            context.object.data.materials.append(mat)
        
        x_middle = 0
        x_gap = 360
        y_gap = 300
        y_top = 600
        context.object.active_material.use_nodes = True
        tree = context.object.active_material.node_tree
        nodes = context.object.active_material.node_tree.nodes
        node = self.createOrGetNode(context, "ShaderNodeTexImage", "Image"+self.MapName)
        node.location.x = x_middle + (-x_gap * 2)
        node.location.y = y_top + (-y_gap * ["BaseColor", "AmbientOcclusion", "Metallic", "Roughness", "Opacity", "Normal", "Height"].index(self.MapName))

        node.image = Image
        outputnode = nodes.get("Principled BSDF")
        mappingnode = self.createOrGetNode(context, "ShaderNodeMapping", "Mapping")
        mappingnode.location.x = x_middle + (-x_gap * 3)
        mappingnode.location.y = y_top + (-y_gap * 3.5)
        coordsnode = self.createOrGetNode(context, "ShaderNodeTexCoord", "Coords")
        coordsnode.location.x = x_middle + (-x_gap * 4)
        coordsnode.location.y = y_top + (-y_gap * 3.5)
        
        tree.links.new(coordsnode.outputs[2], mappingnode.inputs[0])
        tree.links.new(mappingnode.outputs[0], node.inputs[0])

        if str(self.MapName) == "BaseColor":
            mixrgbnode = self.createOrGetNode(context, "ShaderNodeMixRGB", "MixRGBAO")
            aonode = nodes.get("ImageAmbientOcclusion", None)
            mixrgbnode.location.x =  x_middle + (-x_gap * 1)
            mixrgbnode.location.y = y_top + (-y_gap * 0)
            mixrgbnode.blend_type = "MULTIPLY"
            mixrgbnode.inputs[0].default_value = context.scene.prop_AOFac
            tree.links.new(node.outputs[0], mixrgbnode.inputs[1])
            tree.links.new(mixrgbnode.outputs[0], outputnode.inputs[0])

            if aonode is not None:
                tree.links.new(aonode.outputs[0], mixrgbnode.inputs[2])
            
        if str(self.MapName) == "AmbientOcclusion":
            mixrgbnode = self.createOrGetNode(context, "ShaderNodeMixRGB", "MixRGBAO")
            basenode = nodes.get("ImageBaseColor", None)
            mixrgbnode.location.x =  x_middle + (-x_gap * 1)
            mixrgbnode.location.y = y_top + (-y_gap * 0)
            tree.links.new(node.outputs[0], mixrgbnode.inputs[2])
            tree.links.new(mixrgbnode.outputs[0], outputnode.inputs[0])

            if basenode is not None:
                tree.links.new(basenode.outputs[0], mixrgbnode.inputs[1])

        if str(self.MapName) == "Roughness":
            gammanode = self.createOrGetNode(context, "ShaderNodeGamma", "Gamma"+self.MapName)
            gammanode.location.x =  x_middle + (-x_gap * 1)
            gammanode.location.y = y_top + (-y_gap * 3)
            tree.links.new(node.outputs[0], gammanode.inputs[0])
            tree.links.new(gammanode.outputs[0], outputnode.inputs["Roughness"])


        if str(self.MapName) == "Normal":
            bumpnode = self.createOrGetNode(context, "ShaderNodeBump", "Bump"+self.MapName)
            bumpnode.location.x =  x_middle + (-x_gap * 1)
            bumpnode.location.y = y_top + (-y_gap * 5)
            tree.links.new(node.outputs[0], bumpnode.inputs["Height"])
            tree.links.new(bumpnode.outputs[0], outputnode.inputs["Normal"])


        if str(self.MapName) == "Metallic":
            gammanode = self.createOrGetNode(context, "ShaderNodeGamma", "Gamma"+self.MapName)
            gammanode.location.x =  x_middle + (-x_gap * 1)
            gammanode.location.y = y_top + (-y_gap * 2)
            tree.links.new(node.outputs[0], gammanode.inputs[0])
            tree.links.new(gammanode.outputs[0], outputnode.inputs["Metallic"])


        if str(self.MapName) == "Opacity":
            gammanode = self.createOrGetNode(context, "ShaderNodeGamma", "Gamma"+self.MapName)
            gammanode.location.x =  x_middle + (-x_gap * 1)
            gammanode.location.y = y_top + (-y_gap * 4)
            tree.links.new(node.outputs[0], gammanode.inputs[0])
            tree.links.new(gammanode.outputs[0], outputnode.inputs["Alpha"])


        if str(self.MapName) == "Height":
            Texture = bpy.data.textures.get(FullMaterialName, None)
            if Texture is None:
                Texture = bpy.data.textures.new(FullMaterialName, "IMAGE")
            Texture.image = Image
            Texture.name = FullMaterialName

            if context.object.modifiers.get("Displace", None) is None:
                context.object.modifiers.new("Displace", "DISPLACE")
            
            context.object.modifiers["Displace"].texture = Texture
            context.object.modifiers["Displace"].texture_coords = "UV"
            context.object.modifiers["Displace"].texture.extension = "REPEAT"

        return {'FINISHED'}
