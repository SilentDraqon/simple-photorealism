################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Addon Operators
from ..operators.materialcreate import OperatorMaterialCreate
from ..operators.materialrename import OperatorMaterialRename
from ..operators.materialremove import OperatorMaterialRemove

################################################################################
#   Main Panel
################################################################################

class MainPanel(Panel):
    bl_idname = "OBJECT_PT_PANEL_SIMPLE_PBR"
    bl_label = "Simple Photorealism"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        panel = self.layout
        if context.object.active_material is None:
            row = panel.row()
            row.label(text="Create Material to use Photorealism Settings.")
            row = panel.row()
            row.prop(context.scene, "prop_MatName")
            row.operator(OperatorMaterialCreate.bl_idname, text="Create Material", icon="IMAGE")
        if context.object.active_material is not None:
            row = panel.row()
            row.prop(context.scene, "prop_MatName")
            row.operator(OperatorMaterialRename.bl_idname, text="Rename Material", icon="IMAGE")
            row = panel.row()
            row.operator(OperatorMaterialRemove.bl_idname, text="Remove Material", icon="IMAGE").unlinkOnly = False
            row = panel.row()
        return
