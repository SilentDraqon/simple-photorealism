################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Getter
from ..getter.anyNodeExists import anyNodeExists

################################################################################
#   Material Settings Sub-Panel
################################################################################

class SubPanelMaterialSettings_Nodes(Panel):
    bl_idname = "OBJECT_PT_PANEL_SUBMATERIAL_NODES_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_MAINMATERIAL_SIMPLE_PBR"
    bl_label = "Nodes"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None and context.object.active_material is not None and anyNodeExists(context, "node") == True

    def draw(self, context):
        panel = self.layout
        row = panel.row()
        if(context.object.active_material.node_tree.nodes.get("ImageBaseColor", None) is not None 
        and context.object.active_material.node_tree.nodes.get("ImageAmbientOcclusion", None) is not None):
            panel.prop(context.scene, "prop_AOFac")
        if(context.object.active_material.node_tree.nodes.get("ImageRoughness", None) is not None):
            panel.prop(context.scene, "prop_RoughnessGamma")
        if(context.object.active_material.node_tree.nodes.get("ImageNormal", None) is not None):
            panel.prop(context.scene, "prop_BumpInvert")
            panel.prop(context.scene, "prop_BumpStrength")
            panel.prop(context.scene, "prop_BumpDistance")
        if(context.object.active_material.node_tree.nodes.get("ImageMetallic", None) is not None):
            panel.prop(context.scene, "prop_MetallicGamma")
        if(context.object.active_material.node_tree.nodes.get("ImageOpacity", None) is not None):
            panel.prop(context.scene, "prop_OpacityGamma")
