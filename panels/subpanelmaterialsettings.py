################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Getter
from ..getter.anyNodeExists import anyNodeExists

################################################################################
#   Material Settings Sub-Panel
################################################################################

class SubPanelMaterialSettings(Panel):
    bl_idname = "OBJECT_PT_PANEL_MAINMATERIAL_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_SIMPLE_PBR"
    bl_label = "Material Settings"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None and context.object.active_material is not None and anyNodeExists(context, "texture") == True

    def draw(self, context):
        panel = self.layout
        