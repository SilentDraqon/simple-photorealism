################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel


################################################################################
#   Material Settings Sub-Panel
################################################################################

class SubPanelMaterialSettings_Modifiers(Panel):
    bl_idname = "OBJECT_PT_PANEL_SUBMATERIAL_MODIFIERS_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_MAINMATERIAL_SIMPLE_PBR"
    bl_label = "Modifiers"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None and context.scene.prop_IsHighDetail

    def draw(self, context):
        panel = self.layout
        if(context.object.active_material) is not None:
            if(context.object.active_material.node_tree.nodes.get("ImageHeight", None) is not None):
                panel.prop(context.scene, "prop_DisplaceStrength")
                panel.prop(context.scene, "prop_DisplaceMidlevel")
