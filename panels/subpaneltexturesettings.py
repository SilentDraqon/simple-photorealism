################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Addon Operators
from ..operators.filebrowser import OperatorFileBrowser
from ..operators.fileremove import OperatorFileRemove


################################################################################
#   Texture Settings Sub-Panel
################################################################################

class SubPanelTextureSettings(Panel):
    bl_idname = "OBJECT_PT_PANEL_MAINTEXTURE_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_SIMPLE_PBR"
    bl_label = "Manage Textures"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None and context.object.active_material is not None

    def draw(self, context):
        panel = self.layout
        panel.prop(context.scene, "prop_MapDropDown")
        if(context.scene.prop_MapDropDown == "BaseColor" or context.scene.prop_MapDropDown == "AmbientOcclusion" or context.scene.prop_MapDropDown == "Roughness" or context.scene.prop_MapDropDown == "Normal" or context.scene.prop_MapDropDown == "Metallic" or context.scene.prop_MapDropDown == "Height" or context.scene.prop_MapDropDown == "Opacity"):
            if(context.object.active_material.node_tree.nodes.get("Image"+context.scene.prop_MapDropDown, None) is not None):
                row = panel.row()
                row.operator(OperatorFileBrowser.bl_idname, text="Override Map", icon="IMAGE").MapName = context.scene.prop_MapDropDown
                row.operator(OperatorFileRemove.bl_idname, text="Remove Map", icon="CANCEL").MapName = context.scene.prop_MapDropDown
            else:
                panel.operator(OperatorFileBrowser.bl_idname, text="Add Map", icon="IMAGE").MapName = context.scene.prop_MapDropDown