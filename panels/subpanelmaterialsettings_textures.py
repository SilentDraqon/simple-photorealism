################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Getter
from ..getter.anyNodeExists import anyNodeExists

################################################################################
#   Material Settings Sub-Panel
################################################################################

class SubPanelMaterialSettings_Textures(Panel):
    bl_idname = "OBJECT_PT_PANEL_SUBMATERIAL_TEXTURES_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_MAINMATERIAL_SIMPLE_PBR"
    bl_label = "Textures"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None and context.object.active_material is not None and anyNodeExists(context, "texture") == True

    def draw(self, context):
        panel = self.layout
        if context.object.active_material is not None:
            if(context.object.active_material.node_tree.nodes.get("Mapping", None) is not None) or (context.object.modifiers.get("Displace", None) is not None):
                row = panel.row()
                row.prop(context.scene, "prop_TexScaleX")
                row.prop(context.scene, "prop_TexScaleY")
                row = panel.row()
                row.prop(context.scene, "prop_TexOffsetX")
                row.prop(context.scene, "prop_TexOffsetY")
            else:
                panel.label(text="This object must have a material first.")
        elif (context.object.modifiers is not None):
            if (context.object.modifiers.get("Displace", None) is not None):
                row = panel.row()
                row.prop(context.scene, "prop_TexScaleX")
                row.prop(context.scene, "prop_TexScaleY")
                row = panel.row()
                row.prop(context.scene, "prop_TexOffsetX")
                row.prop(context.scene, "prop_TexOffsetY")
            else:
                panel.label(text="This object must have a material first.")
        else:
            panel.label(text="This object must have a material first.")
